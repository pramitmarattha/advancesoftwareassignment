﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;




namespace PramitAdvanceSoftwareComp1Assignment
{
    public partial class Form1 : Form
    {
        bool paint = true;
        bool commandLine = true;
        Graphics shapeVector;   //craeting a shapeVector object
        int k = 0;
        int l = 0;
        int radius= 0;

        public Form1()
        {
            InitializeComponent();
           shapeVector = DrawingBoard.CreateGraphics();
        }



        /// <summary>
        /// Opens the dialog box and displays the files when file menu item is pressed
        /// </summary>

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDial = new OpenFileDialog();          
            openDial.Filter = "Text Files(*.txt)|*.txt| All Files(*.*)|*.*"; //Option to Filter out the txt files or to show all files
            if (openDial.ShowDialog() == DialogResult.OK)
            {
                StreamReader openStream = new StreamReader(File.OpenRead(openDial.FileName));
                MultipleLine.Text = openStream.ReadToEnd(); 
                openStream.Dispose(); //It cleares from the memory
            }

        }



        /// <summary>
        /// Opens the dialog box and displays the files to be opened when open button is pressed 
        /// </summary>

        private void openButtonMultiple_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDial = new OpenFileDialog();
            openDial.Filter = "Text Files(*.txt)|*.txt| All Files(*.*)|*.*"; //Option to Filter out the txt files or to show all files
            if (openDial.ShowDialog() == DialogResult.OK)
            {
                StreamReader openStream = new StreamReader(File.OpenRead(openDial.FileName));
                MultipleLine.Text = openStream.ReadToEnd();
                openStream.Dispose();
            }
        }


        /// <summary>
        /// Displays the dialog box and provide the option to save the file  when save button is pressed 
        /// </summary>

        private void SaveMultipleCommand_Click(object sender, EventArgs e)
        {         
                SaveFileDialog saveDial = new SaveFileDialog();  //saveDial object is created
                saveDial.Filter = "Text Files(*.txt)|*.txt| All Files(*.*)|*.*"; // Option to save the file in txt or any otehr format
                if (saveDial.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    StreamWriter saveStream = new StreamWriter(File.Create(saveDial.FileName));  //writes into the file
                    saveStream.Write(MultipleLine.Text);
                    saveStream.Dispose();       //clears the memory 
                }     
                
        }




        /// <summary>
        /// Displays the dialog box and provide the option to save the file when save menu item is clicked
        /// </summary>

        private void saveMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveDial = new SaveFileDialog();   //saveDial object is created
            saveDial.Filter = "Text Files(*.txt)|*.txt| All Files(*.*)|*.*";  // Option to save the file in txt or any otehr format
            if (saveDial.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                StreamWriter saveStream = new StreamWriter(File.Create(saveDial.FileName));   //writes into the file
                saveStream.Write(MultipleLine.Text);
                saveStream.Dispose();   //clears the memory
            }
        }


        /// <summary>
        /// Closes the entire aplication when "Exit" button is pressed
        /// </summary>
        private void exitToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }



        /// <summary>
        /// Drawing board
        /// </summary>

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }


        /// <summary>
        /// Closes the entire aplication when "Exit" menu-item is pressed
        /// </summary>
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }


        /// <summary>
        /// creating a method
        /// </summary>
        public void multipleLineMethod()
        {
            string getText = MultipleLine.Text;
            Check v = new Check();
            String[] result = v.ValidationCheck(getText);
            try
            {
                if (result[0] == "MoveTo")
                {
                    int a = Convert.ToInt32(result[1]);
                    int b = Convert.ToInt32(result[2]);
                    k = a;
                    l = b;
                 
                }
                else if (result[0] == "DrawTo")
                {                
                    ShapeFactory s4 = new ShapeFactory();
                    shape sh4 = s4.getShape(result[0]);
                    sh4.drawShape(result, shapeVector, k, l);
                  
                }


                else if (result[0] == "rectangle")
                {
                    ShapeFactory s1 = new ShapeFactory();
                    shape sh = s1.getShape(result[0]);
                    sh.drawShape(result, shapeVector, k, l);
                }


                else if (result[0] == "circle")
                {
                    ShapeFactory s2 = new ShapeFactory();
                    shape sh2 = s2.getShape(result[0]);
                    sh2.drawShape(result, shapeVector, k, l);

                }
                else if (result[0] == "triangle")
                {
                    ShapeFactory s2 = new ShapeFactory();
                    shape sh3 = s2.getShape(result[0]);
                    sh3.drawShape(result, shapeVector, k, l);
                }
            }

            catch
            {

                MessageBox.Show("Invalid Command !!");  //Displays message box Incorrect command is entered
            }

        }


        /// <summary>
        /// Textbox that can executes multiple line command
        /// </summary>

        private void MultipleLine_TextChanged(object sender, EventArgs e)
        {
            
        }


        private void button1_Click(object sender, EventArgs e)
        {

            textBox1.Text = String.Empty;
            MessageBox.Show("Command Cleared");
            // commandLine = false;
            // textBox1.Refresh();
        }






        private void Form1_Load(object sender, EventArgs e)
        {

        }



        /// <summary>
        /// Refreshes and clean the picture box or drawing board into its initial form
        /// </summary>


        private void ClearDrawing_Click(object sender, EventArgs e)
        {
            paint = false;
            DrawingBoard.Refresh();
            MessageBox.Show("Everything Cleared");
        }



        /// <summary>
        /// Textbox to type command
        /// </summary>




        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }




        /// <summary>
        /// Resets the cursor position to its initial value 1, 1
        /// </summary>

        private void ResetButton_Click(object sender, EventArgs e)
        {         
            string resetButn = "moveTo 1,1";
            Check v = new Check();
            String[] result = v.ValidationCheck(resetButn); 
            if (result[0] == "MoveTo")
            {              
                int pos1 = 1;
                int pos2 = 1;
                k = 1;
                l = 1;
            }

            paint = false;                          
            MessageBox.Show("Everything is Reseted");  //Displays a Pop up message  
        }



        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }



        /// <summary>
        /// ///////////////////////////////////////////////////////////////////
        /// Creating a run method
        /// </summary>
        //
        public void runMethod()
        {
         
        }

        /// <summary>
        /// Reads from the user input given in textbox and validates it.
        /// </summary>

        public void Run_Click(object sender, EventArgs e)
            {
                string getText = textBox1.Text;
                Check validShape = new Check();   //Validates the input from textbox by sending it in check class
                String[] result = validShape.ValidationCheck(getText);
          try
            {
               if (result[0] == "MoveTo")
                {
                    int a = Convert.ToInt32(result[1]);
                    int b = Convert.ToInt32(result[2]);
                    k = a;
                    l = b;
                    MessageBox.Show("Cursor moved to " + a + "," + b + " position");
                }
                else if (result[0] == "DrawTo")
                {
                    ShapeFactory s1 = new ShapeFactory();
                    shape sh = s1.getShape(result[0]);
                    sh.drawShape(result, shapeVector, k, l);
                }
                else if (result[0] == "rectangle")
                {
                    ShapeFactory s1 = new ShapeFactory();
                    shape sh = s1.getShape(result[0]);
                    sh.drawShape(result, shapeVector, k, l);
                }
                else if (result[0] == "circle")
                {
                    ShapeFactory s2 = new ShapeFactory();
                    shape sh2 = s2.getShape(result[0]);
                    sh2.drawShape(result, shapeVector, k, l);
                }
                else if (result[0] == "triangle")
                {
                    ShapeFactory s2 = new ShapeFactory();
                    shape sh3 = s2.getShape(result[0]);
                    sh3.drawShape(result, shapeVector, k, l);
                }
            }
            catch
            {     
                    MessageBox.Show("Invalid command");             
            }
            }        
        private void keyStrokeShortcutMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Press '''enter''' to execute command \r\n ");
            
        }






        private void aboutMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Graphical programming language application \r\n created by Pramit marattha" );
        }







        private void Run_KeyUp(object sender, KeyEventArgs e)
        {
           
        }






        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
              //  runMethod();               
            }
        }






    

        private void RunButtonMultipleLine_Click(object sender, EventArgs e)
        {
            ///////////////////////////////////////////////////////
            ///
          //  string getText = MultipleLine.Text;
          //  Validate v = new Validate();
          //  String[] result = v.ValidationCheck(getText);


            ////////////////////////////////////////////////////////////////



            if (!string.IsNullOrEmpty(Convert.ToString(MultipleLine.Text)))
            {
                string IdOrder = Convert.ToString(MultipleLine.Text.Trim());
                string[] ArrIdOrders = Regex.Split(IdOrder, "\n");
                for (int i = 0; i < ArrIdOrders.Length; i++)
                {
                    Check validcode = new Check();
                      String[] result = validcode.ValidationCheck(ArrIdOrders[i]);
                  //  MessageBox.Show(ArrIdOrders[i]);
                    //  }
                    try
                    {
                        if (result[0] == "MoveTo")
                        {

                            int a = Convert.ToInt32(result[1]);
                            int b = Convert.ToInt32(result[2]);
                            k = a;
                            l = b;
                            MessageBox.Show("Cursor moved to " + a + "," + b + " position");
                        }
                        else if (result[0] == "DrawTo")
                        {

                            // int a = Convert.ToInt32(result[1]);
                            // int b = Convert.ToInt32(result[2]);
                            ShapeFactory s4 = new ShapeFactory();
                            shape sh4 = s4.getShape(result[0]);
                            sh4.drawShape(result, shapeVector, k, l);
                            //  k = a;
                            // l = b;
                            //  MessageBox.Show("Draw To Activated ");
                        }
                        // else if (result[0] == "DrawTo")
                        //  {

                        //    int a = Convert.ToInt32(result[1]);
                        //      int b = Convert.ToInt32(result[2]);
                        //     k = a;
                        //      l = b;
                        // MessageBox.Show("Draw To Activated ");
                   // }


                        else if (result[0] == "rectangle")
                        {
                            ShapeFactory s1 = new ShapeFactory();
                            shape sh = s1.getShape(result[0]);
                            sh.drawShape(result, shapeVector, k, l);
                        }

                        else if (result[0] == "circle")
                        {
                            ShapeFactory s2 = new ShapeFactory();
                            shape sh2 = s2.getShape(result[0]);
                            sh2.drawShape(result, shapeVector, k, l);

                        }
                        else if (result[0] == "triangle")
                        {
                            ShapeFactory s2 = new ShapeFactory();
                            shape sh3 = s2.getShape(result[0]);
                            sh3.drawShape(result, shapeVector, k, l);
                        }
                    }
                    catch
                    {

                        MessageBox.Show("Invalid command");
                    }
                }
            }
        }







        private void ClearMutipleLine_Click(object sender, EventArgs e)
        {
            MultipleLine.Text = String.Empty;
            MessageBox.Show("Command Cleared");
        }


    }
}


