﻿
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PramitAdvanceSoftwareComp1Assignment
{
    /// <summary>
    /// Triangle class implements the "shape" interface
    /// </summary>
    class Triangle : shape {
        public void drawShape(string[] inputpoint, Graphics objGraphic, int k, int l){            
            int base1 = Convert.ToInt32(inputpoint[1]);
            int adj = Convert.ToInt32(inputpoint[2]);
            int hyp = Convert.ToInt32(inputpoint[3]);
            int trianglePosPoint = 0;
            int triSidePoint = 0;
            if (base1 + adj > hyp && base1 + hyp > adj && adj + hyp > base1){
                /// <summary>
                /// Triangle class implements the "shape" interface
                /// </summary>
                if (adj > base1){
                    if (hyp > adj){
                        triSidePoint = hyp;
                        hyp = base1;
                        base1 = triSidePoint;}
                    else{
                        triSidePoint = adj;
                        adj = base1;
                        base1 = triSidePoint;} }
                if (hyp > base1){
                    triSidePoint = hyp;
                    hyp = base1;
                    base1 = triSidePoint;}
                double sides = (base1 + adj + hyp) / 2; // formula of area of triangle
                double area = Math.Sqrt(sides * (sides - base1) * (sides - adj) * (sides - hyp));    // aea of triangle(s(s-a)(s-b)(s-c))^1/2
                double h = 2 * area / base1;
                double point = (h * h) - (adj * adj);
                int sh = Convert.ToInt32(h);
                if (point < 0){
                    point *= (-1);
                    double temp = Math.Sqrt(point);
                    trianglePosPoint = Convert.ToInt32(temp); }
                else{ double temp = Math.Sqrt(point);
                 trianglePosPoint = Convert.ToInt32(temp);}
                Point[] points = new Point[3];
                points[0] = new Point(k, l);
                points[1] = new Point(k, base1);
                points[2] = new Point(sh, trianglePosPoint);
                Pen pen = new Pen(Color.Black, 4);
                objGraphic.DrawLine(pen, points[0], points[1]);
                objGraphic.DrawLine(pen, points[1], points[2]);
                objGraphic.DrawLine(pen, points[0], points[2]);}
            else { MessageBox.Show("Triangle cannot be formed");}
        }
    }}








