﻿namespace PramitAdvanceSoftwareComp1Assignment
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DrawingBoard = new System.Windows.Forms.Panel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.RunButton = new System.Windows.Forms.Button();
            this.ClearCommandButton = new System.Windows.Forms.Button();
            this.MultipleLine = new System.Windows.Forms.RichTextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OpenMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.keyStrokeShortcutMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openButtonMultiple = new System.Windows.Forms.Button();
            this.SaveMultipleCommand = new System.Windows.Forms.Button();
            this.RunButtonMultipleLine = new System.Windows.Forms.Button();
            this.ClearDrawing = new System.Windows.Forms.Button();
            this.ResetButton = new System.Windows.Forms.Button();
            this.ClearMutipleLine = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // DrawingBoard
            // 
            this.DrawingBoard.BackColor = System.Drawing.Color.LightSkyBlue;
            this.DrawingBoard.Location = new System.Drawing.Point(31, 79);
            this.DrawingBoard.Margin = new System.Windows.Forms.Padding(2);
            this.DrawingBoard.Name = "DrawingBoard";
            this.DrawingBoard.Size = new System.Drawing.Size(711, 283);
            this.DrawingBoard.TabIndex = 0;
            this.DrawingBoard.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.White;
            this.textBox1.Font = new System.Drawing.Font("Arial", 9.900001F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(31, 367);
            this.textBox1.Margin = new System.Windows.Forms.Padding(2);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(330, 26);
            this.textBox1.TabIndex = 1;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.textBox1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyUp);
            // 
            // RunButton
            // 
            this.RunButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.RunButton.Location = new System.Drawing.Point(31, 397);
            this.RunButton.Margin = new System.Windows.Forms.Padding(2);
            this.RunButton.Name = "RunButton";
            this.RunButton.Size = new System.Drawing.Size(89, 28);
            this.RunButton.TabIndex = 2;
            this.RunButton.Text = "Run";
            this.RunButton.UseVisualStyleBackColor = false;
            this.RunButton.Click += new System.EventHandler(this.Run_Click);
            this.RunButton.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Run_KeyUp);
            // 
            // ClearCommandButton
            // 
            this.ClearCommandButton.BackColor = System.Drawing.Color.Tomato;
            this.ClearCommandButton.Location = new System.Drawing.Point(125, 397);
            this.ClearCommandButton.Name = "ClearCommandButton";
            this.ClearCommandButton.Size = new System.Drawing.Size(97, 28);
            this.ClearCommandButton.TabIndex = 3;
            this.ClearCommandButton.Text = "Clear ";
            this.ClearCommandButton.UseVisualStyleBackColor = false;
            this.ClearCommandButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // MultipleLine
            // 
            this.MultipleLine.Location = new System.Drawing.Point(458, 417);
            this.MultipleLine.Name = "MultipleLine";
            this.MultipleLine.Size = new System.Drawing.Size(284, 141);
            this.MultipleLine.TabIndex = 4;
            this.MultipleLine.Text = "";
            this.MultipleLine.TextChanged += new System.EventHandler(this.MultipleLine_TextChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpMenuItem,
            this.exitToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(756, 28);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OpenMenuItem,
            this.saveMenuItem,
            this.exitMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // OpenMenuItem
            // 
            this.OpenMenuItem.Name = "OpenMenuItem";
            this.OpenMenuItem.Size = new System.Drawing.Size(216, 26);
            this.OpenMenuItem.Text = "Open";
            this.OpenMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveMenuItem
            // 
            this.saveMenuItem.Name = "saveMenuItem";
            this.saveMenuItem.Size = new System.Drawing.Size(216, 26);
            this.saveMenuItem.Text = "Save";
            this.saveMenuItem.Click += new System.EventHandler(this.saveMenuItem_Click);
            // 
            // exitMenuItem
            // 
            this.exitMenuItem.Name = "exitMenuItem";
            this.exitMenuItem.Size = new System.Drawing.Size(216, 26);
            this.exitMenuItem.Text = "Exit";
            this.exitMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // helpMenuItem
            // 
            this.helpMenuItem.BackColor = System.Drawing.Color.LemonChiffon;
            this.helpMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.keyStrokeShortcutMenuItem,
            this.aboutMenuItem});
            this.helpMenuItem.Name = "helpMenuItem";
            this.helpMenuItem.Size = new System.Drawing.Size(53, 24);
            this.helpMenuItem.Text = "Help";
            this.helpMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
            // 
            // keyStrokeShortcutMenuItem
            // 
            this.keyStrokeShortcutMenuItem.Name = "keyStrokeShortcutMenuItem";
            this.keyStrokeShortcutMenuItem.Size = new System.Drawing.Size(209, 26);
            this.keyStrokeShortcutMenuItem.Text = "KeyStroke Shortcut";
            this.keyStrokeShortcutMenuItem.Click += new System.EventHandler(this.keyStrokeShortcutMenuItem_Click);
            // 
            // aboutMenuItem
            // 
            this.aboutMenuItem.Name = "aboutMenuItem";
            this.aboutMenuItem.Size = new System.Drawing.Size(209, 26);
            this.aboutMenuItem.Text = "About";
            this.aboutMenuItem.Click += new System.EventHandler(this.aboutMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.BackColor = System.Drawing.Color.IndianRed;
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(45, 24);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click_1);
            // 
            // openButtonMultiple
            // 
            this.openButtonMultiple.BackColor = System.Drawing.Color.PaleTurquoise;
            this.openButtonMultiple.Location = new System.Drawing.Point(458, 381);
            this.openButtonMultiple.Name = "openButtonMultiple";
            this.openButtonMultiple.Size = new System.Drawing.Size(75, 30);
            this.openButtonMultiple.TabIndex = 6;
            this.openButtonMultiple.Text = "Open";
            this.openButtonMultiple.UseVisualStyleBackColor = false;
            this.openButtonMultiple.Click += new System.EventHandler(this.openButtonMultiple_Click);
            // 
            // SaveMultipleCommand
            // 
            this.SaveMultipleCommand.BackColor = System.Drawing.Color.MediumTurquoise;
            this.SaveMultipleCommand.Location = new System.Drawing.Point(539, 380);
            this.SaveMultipleCommand.Name = "SaveMultipleCommand";
            this.SaveMultipleCommand.Size = new System.Drawing.Size(65, 31);
            this.SaveMultipleCommand.TabIndex = 7;
            this.SaveMultipleCommand.Text = "Save";
            this.SaveMultipleCommand.UseVisualStyleBackColor = false;
            this.SaveMultipleCommand.Click += new System.EventHandler(this.SaveMultipleCommand_Click);
            // 
            // RunButtonMultipleLine
            // 
            this.RunButtonMultipleLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.RunButtonMultipleLine.Location = new System.Drawing.Point(552, 564);
            this.RunButtonMultipleLine.Name = "RunButtonMultipleLine";
            this.RunButtonMultipleLine.Size = new System.Drawing.Size(96, 32);
            this.RunButtonMultipleLine.TabIndex = 8;
            this.RunButtonMultipleLine.Text = "Run";
            this.RunButtonMultipleLine.UseVisualStyleBackColor = false;
            this.RunButtonMultipleLine.Click += new System.EventHandler(this.RunButtonMultipleLine_Click);
            // 
            // ClearDrawing
            // 
            this.ClearDrawing.BackColor = System.Drawing.Color.IndianRed;
            this.ClearDrawing.Location = new System.Drawing.Point(169, 35);
            this.ClearDrawing.Name = "ClearDrawing";
            this.ClearDrawing.Size = new System.Drawing.Size(118, 39);
            this.ClearDrawing.TabIndex = 9;
            this.ClearDrawing.Text = "Clear drawing";
            this.ClearDrawing.UseVisualStyleBackColor = false;
            this.ClearDrawing.Click += new System.EventHandler(this.ClearDrawing_Click);
            // 
            // ResetButton
            // 
            this.ResetButton.BackColor = System.Drawing.Color.IndianRed;
            this.ResetButton.Location = new System.Drawing.Point(31, 35);
            this.ResetButton.Name = "ResetButton";
            this.ResetButton.Size = new System.Drawing.Size(132, 39);
            this.ResetButton.TabIndex = 10;
            this.ResetButton.Text = "Reset";
            this.ResetButton.UseVisualStyleBackColor = false;
            this.ResetButton.Click += new System.EventHandler(this.ResetButton_Click);
            // 
            // ClearMutipleLine
            // 
            this.ClearMutipleLine.BackColor = System.Drawing.Color.IndianRed;
            this.ClearMutipleLine.Location = new System.Drawing.Point(654, 565);
            this.ClearMutipleLine.Name = "ClearMutipleLine";
            this.ClearMutipleLine.Size = new System.Drawing.Size(88, 31);
            this.ClearMutipleLine.TabIndex = 11;
            this.ClearMutipleLine.Text = "Clear";
            this.ClearMutipleLine.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ClearMutipleLine.UseVisualStyleBackColor = false;
            this.ClearMutipleLine.Click += new System.EventHandler(this.ClearMutipleLine_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(756, 608);
            this.Controls.Add(this.ClearMutipleLine);
            this.Controls.Add(this.ResetButton);
            this.Controls.Add(this.ClearDrawing);
            this.Controls.Add(this.RunButtonMultipleLine);
            this.Controls.Add(this.SaveMultipleCommand);
            this.Controls.Add(this.openButtonMultiple);
            this.Controls.Add(this.MultipleLine);
            this.Controls.Add(this.ClearCommandButton);
            this.Controls.Add(this.RunButton);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.DrawingBoard);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel DrawingBoard;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button RunButton;
        private System.Windows.Forms.Button ClearCommandButton;
        private System.Windows.Forms.RichTextBox MultipleLine;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem OpenMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Button openButtonMultiple;
        private System.Windows.Forms.Button SaveMultipleCommand;
        private System.Windows.Forms.Button RunButtonMultipleLine;
        private System.Windows.Forms.Button ClearDrawing;
        private System.Windows.Forms.ToolStripMenuItem helpMenuItem;
        private System.Windows.Forms.ToolStripMenuItem keyStrokeShortcutMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutMenuItem;
        private System.Windows.Forms.Button ResetButton;
        private System.Windows.Forms.Button ClearMutipleLine;
    }
}

