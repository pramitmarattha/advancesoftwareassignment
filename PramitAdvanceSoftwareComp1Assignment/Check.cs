﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PramitAdvanceSoftwareComp1Assignment
{
    /*
    * This is check class which checks and validate the format of user input
    */
    public class Check
    {
        public string[] ValidationCheck(String validationSplit)
        {      
            /// <summary>
            /// check class to check the validation of the code
            /// </summary> 
                string[] validateReturn = { };
                string[] validValueEnter = validationSplit.Split(',', ' ');
                try
                {
                    string commandInput = validValueEnter[0].ToUpper(); //converts the entered string to uppercase
                    
                        /// <summary>
                        /// Moves the cursor to the given parameter provided by the user 
                        /// </summary>
                        if (commandInput.Equals("MOVETO"))
                        {
                            if (validValueEnter.Length == 3)
                            {
                                int positionInitial1 = Convert.ToInt32(validValueEnter[1]);
                                int positionInitial2 = Convert.ToInt32(validValueEnter[2]);
                                string pos1 = Convert.ToString(positionInitial1);
                                string pos2 = Convert.ToString(positionInitial2);
                                string[] moveToformat = { "MoveTo", pos1, pos2 };
                                validateReturn = moveToformat;
                            }                     
                    else
                        {
                            string[] sPoint = { "1", "1" };
                            validateReturn = sPoint;
                            MessageBox.Show("MoveTo Command Invalid");// displays message box
                        }
                    }
                        /// <summary>
                        /// Draws the line according to user input
                        /// </summary>
                        if (commandInput.Equals("DRAWTO"))
                        {
                            if (validValueEnter.Length == 3)
                            {
                                int positionInitial1 = Convert.ToInt32(validValueEnter[1]);
                                int positionInitial2 = Convert.ToInt32(validValueEnter[2]);
                                string linepos1 = Convert.ToString(positionInitial1);
                                string linepos2 = Convert.ToString(positionInitial2);
                                string[] drawToformat = { "DrawTo", linepos1, linepos2 };
                                validateReturn = drawToformat;
                            }
                    else
                    {
                        string[] sPoint = { "1", "1" };
                        validateReturn = sPoint;
                        MessageBox.Show("DrawTo command Invalid");// displays message box
                    }
                }
                /// <summary>
                /// Draws Rectangle according to given width and height 
                /// </summary>
                /// 
                if (commandInput.Equals("RECTANGLE"))
                        {
                            if (validValueEnter.Length == 3)
                            {
                                int x = Convert.ToInt32(validValueEnter[1]);
                                int y = Convert.ToInt32(validValueEnter[2]);
                                string width = Convert.ToString(x);
                                string height = Convert.ToString(y);
                                string[] rectangleFormat = { "rectangle", width, height };
                                validateReturn = rectangleFormat;
                            }
                            else
                            {
                                string[] sPoint = { "1", "1" };
                                validateReturn = sPoint;
                                MessageBox.Show("Rectangle Command Invalid");// displays message box
                            }
                        }      
                /// <summary>
                /// Draws Circle according to given command 
                /// </summary>
                /// 
                if (commandInput.Equals("CIRCLE"))
                        {
                            if (validValueEnter.Length == 2)
                            {
                                int x = Convert.ToInt32(validValueEnter[1]);
                                string radius = Convert.ToString(x * 2);
                                string[] circleFormat = { "circle", radius };
                                validateReturn = circleFormat;
                            }
                else
                {
                    string[] sPoint = { "1", "1" };
                    validateReturn = sPoint;
                    MessageBox.Show("Circle Command Invalid");// displays message box

                }
            }
                /// <summary>
                /// Draws Triangle according to given command 
                /// </summary>
                ///
                if (commandInput.Equals("TRIANGLE"))
                        {
                            if (validValueEnter.Length == 4)
                            {
                                int point1 = Convert.ToInt32(validValueEnter[1]);
                                int point2 = Convert.ToInt32(validValueEnter[2]);
                                int point3 = Convert.ToInt32(validValueEnter[3]);
                                string base1 = Convert.ToString(point1);
                                string adj = Convert.ToString(point2);
                                string hyp = Convert.ToString(point3);
                                string[] triangleFormat = { "triangle", base1, adj, hyp };
                                validateReturn = triangleFormat;
                            }
                    else
                    {
                        string[] sPoint = { "1", "1" };
                        validateReturn = sPoint;
                        MessageBox.Show("Triangle Command Invalid");// displays message box
                    }
                }          
                }
                catch (FormatException e) //format exception is thrown when the format of the argument is invalid
                {
                    string[] exceptcheck = { "1", "1" };  
                    validateReturn = exceptcheck;
                }
                catch (IndexOutOfRangeException e)
                {
                    MessageBox.Show("Invalid");     //Displays a pop up message when Invalid command is entered
                }
                return validateReturn;
            }
        }
    }
    

