﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PramitAdvanceSoftwareComp1Assignment
{
    /// <summary>
    /// Circle class implements the "shape" interface
    /// </summary>
    class Circle : shape  {
        public void drawShape(string[] circlePos, Graphics shapeVector, int k, int l) {
            int inputRadius1 = Convert.ToInt32(circlePos[1]);
            int inputRadius2 = Convert.ToInt32(circlePos[1]);
            Pen p = new Pen(Color.Black, 5);
            shapeVector.DrawEllipse(p, k, l, inputRadius1, inputRadius2);
        }
    }
}
