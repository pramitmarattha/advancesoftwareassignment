﻿using System;
using PramitAdvanceSoftwareComp1Assignment;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestShape.Tests
{
    [TestClass]
    public class UnitTest1
    {

        /// <summary>
        /// Creating a Unit test to test weather the code is valid or not
        /// </summary>


        /// <summary>
        ///Unit Testing of Rectangle
        /// </summary>
        [TestMethod]
        public void Test_Rectangle_is_True()
        {
            //Arrange
            var validReturn = new Check();

            //Act
            string[] validTest = validReturn.ValidationCheck("rectangle 30 40");

            //Assert
            Assert.AreEqual(validTest[0], "rectangle", validTest[1], "30", validTest[2], "40");

        }
        [TestMethod]
        public void Test_Rectangle_is_False()
        {
            //Arrange
            var validReturn = new Check();

            //Act
            string[] result = validReturn.ValidationCheck("rectangle 20");

            //Assert
            Assert.AreEqual(result[0], "1");
        }




        /// <summary>
        /// Unit Testing of Circle
        /// </summary>
        [TestMethod]
        public void Test_Circle_is_True()
        {
            //Arrange
            var validReturn = new Check();

            //Act
            string[] result = validReturn.ValidationCheck("circle 10");

            //Assert
            Assert.AreEqual(result[0], "circle", result[1], "10");

        }
        [TestMethod]
        public void Test_Circle_is_False()
        {

            //Arrange
            var validReturn = new Check();

            //Act
            string[] result = validReturn.ValidationCheck("circle 50 50");

            //Assert
            Assert.AreEqual(result[0], "1");
        }





        /// <summary>
        /// Unit Testing of Triangle
        /// </summary>
        [TestMethod]
        public void Test_Triangle_is_True()
        {

            //Arrange
            var validReturn = new Check();

            //Act
            string[] result = validReturn.ValidationCheck("triangle 40 50 60");

            //Assert
            Assert.AreEqual(result[0], "triangle", result[1], "40", result[2], "50", result[3], "60");

        }
        [TestMethod]
        public void Test_Triangle_is_False()
        {
            //Arrange
            var validReturn = new Check();

            //Act
            string[] result = validReturn.ValidationCheck("triangle 20");

            //Assert
            Assert.AreEqual(result[0], "1");
        }



        /// <summary>
        /// Unit Testing of MoveTo
        /// </summary>
        [TestMethod]
        public void Test_MoveTo_is_True()
        {

            //Arrange
            var validReturn = new Check();

            //Act
            string[] result = validReturn.ValidationCheck("moveto 40 40");

            //Assert
            Assert.AreEqual(result[0], "MoveTo", result[1], "45", result[2], "55");
        }

        [TestMethod]
        public void Test_MoveTo_is_False()
        {
            //Arrange
            var validReturn = new Check();

            //Act
            string[] result = validReturn.ValidationCheck("moveto 10");

            //Assert
            Assert.AreEqual(result[0], "1");
        }



        /// <summary>
        /// Unit Testing of DrawTo
        /// </summary>
        [TestMethod]
        public void Test_DrawTo_is_True()
        {

            //Arrange
            var validReturn = new Check();

            //Act
            string[] result = validReturn.ValidationCheck("drawto 20 30");

            //Assert
            Assert.AreEqual(result[0], "DrawTo", result[1], "20", result[2], "30");

        }
        [TestMethod]
        public void Test_DrawTo_is_False()
        {
            //Arrange 
            var validReturn = new Check();

            //Act
            string[] result = validReturn.ValidationCheck("drawto 20");

            //Assert
            Assert.AreEqual(result[0], "1");

        }







    }
}